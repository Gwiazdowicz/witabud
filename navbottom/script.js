const arrow = document.querySelector(".arrow");
const nav = document.querySelector("nav");
const header = document.querySelector("header");

arrow.addEventListener("click", function () {
    arrow.classList.toggle("on");
    nav.classList.toggle("on");
    header.classList.toggle("on");
})