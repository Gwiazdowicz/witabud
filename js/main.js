var preloaderEl = document.querySelector('#preloader');
btnright = document.getElementById("right-btn");
btnleft = document.getElementById("left-btn");


var color = ["#f368e0","#ee5253","#FFFFFF"];
var i = 0;

btnright.addEventListener("click", function(){
i = i < color.length ? ++i : 0;
document.querySelector("header").style.background = color[i];
})

btnleft.addEventListener("click", function(){
i = i < color.length ? ++i : 0;
document.querySelector("header").style.background = color[i];
})

$(document).ready(function(){
	$('.menu-toggle').click(function(){
		$('nav').toggleClass('active')
	})
	$('.back').click(function(){
		$('nav').toggleClass('active')
	})
})


window.addEventListener('load', function() {
	preloaderEl.classList.add('preloader-hiding');
	
	preloaderEl.addEventListener('transitionend', function() {
		this.classList.add('preloader-hidden');
		this.classList.remove('preloader-hiding');
	});
});